const {By,Key,Builder} = require("selenium-webdriver");
require("chromedriver");
 
async function example(){
 
       var searchString = "qa ";
 
       //To wait for browser to build and launch properly
       let driver = await new Builder().forBrowser("chrome").build();
 
        //To fetch http://google.com from the browser with our code.
        await driver.get("https://www.zippia.com/project-manager-jobs/");

        // var element = driver.findElement(By.xpath("//h2[text()='Project Manager Demographics']"));
        // // const element = driver.findElement(By.xpath("//h2[text()='Project Manager Demographics']"));
        // await driver.executeScript("arguments[0].scrollIntoView(true);", element)
        // await driver.sleep(500);
        
        //To send a search query by passing the value in searchString.
        await driver.findElement(By.xpath("//h2[text()='Project Manager Demographics']/following-sibling::div/div/div/div/div[@class='d-none d-md-flex filter-carousel']/div")).click();
        await driver.sleep(5000);
        // await driver.findElement(By.xpath("(//div[text()='Compare Jobs']/following-sibling::div/form/div/div/input)[1]")).sendKeys(searchString, Key.RETURN);
        await driver.findElement(By.xpath("(//div[contains(text(),'Clear')])[2]")).click();
        //Top Colleges for Project Managers column --> 1st value
        await driver.findElement(By.xpath("//h4[text() = '1. University of Pennsylvania']/parent::div/parent::div/following-sibling::div/button")).click();

        //get attribute value
        var title = await driver.findElement(By.xpath("//p[text()='www.upenn.edu']")).getAttribute();
        console.log('Title is:',title);
                

        await driver.quit();

}
 
example()