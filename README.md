1. VS CODE editor and NODE.JS should be installed in the system
2. Unzip the Assignment_Zippia.rar folder
3. Open the folder in VS Code
4. Prerequisites for Selenium Webdriver with Javascript:
	- NodeJS and NPM
	- Verify if NodeJS and NPM are already installed in your system by running the commands "node -v" and "npm -v" on the terminal
	- run "npm install" if NPM is not installed in your system
5. run "npm init -y" in the terminal
6. Now, install the project dependencies. We need to install Selenium WebDriver and browser drivers.
Run the following command on the terminal to install browser drivers for Chrome and Firefox.
	- run "npm install --save selenium-webdriver chromedriver geckodriver"
7. "node test.js" in the last

Video link is given below:
https://www.screencast.com/t/TT92WhGw
---------------------------------------------------------------------------------------------------------------

What we have covered in the Automation:
1. Search field of Compare Job dropdown button, Clear button, form close under Project Management Demographics module
2. pop-up forms, website link present in the form, under Top Colleges for Project Managers module